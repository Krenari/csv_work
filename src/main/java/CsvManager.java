import com.opencsv.CSVReader;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.stream.IntStream;

public class CsvManager {

    public static void main(String args[]) throws IOException {
//        String fileName = "//home//g3collector//Downloads//test.csv";
//        String newFileName = "//home//g3collector//Downloads//_gentest.csv";


        String fileName = args[0];
        String newFileName = args[1];

        FormattedRow.SEPERATOR = ",";
        Formatter formatter = readCsv(fileName);
        List<String[]> rowsFormatted = formatter.processOldRowsAndReturnToCsvRowNeededFormat();
        insertOrupdateCsv(newFileName,rowsFormatted);
    }


    public static Formatter readCsv(String path) throws IOException {
        Formatter formatter = new Formatter();
        try (CSVReader reader = new CSVReader(new FileReader(path), ';')) {
            final List<String[]> rows = reader.readAll();
            IntStream.range(1,rows.size())
                    .mapToObj(index -> rows.get(index))
                    .forEach(formatter::initialRowsFiller);
        }
        return formatter;
    }


    public static void insertOrupdateCsv(final String fileToUpdate, List<String[]> csvBody) throws IOException {
        final File inputFile = new File(fileToUpdate);

        // Write to CSV file which is open
        com.opencsv.CSVWriter writer = new com.opencsv.CSVWriter(new FileWriter(inputFile));
        writer.writeAll(csvBody);
        writer.flush();
        writer.close();
    }
}