public class FormattedRow {

    public static String SEPERATOR = ",";

    private String product;
    private String categories;
    private String positions;
    private String positionCategories;
    private String matchers;



    public void addCategory(String category){
        if(!category.trim().equals(""))
        this.categories += SEPERATOR + category;
    }

    public void addPosition(String position){
        if(!position.trim().equals(""))
        this.positions  += SEPERATOR + position;
    }

    public void addPositionCategories(String positionCategory){
        if(!positionCategory.trim().equals(""))
        this.positionCategories += SEPERATOR + positionCategory;
    }

    public void addMatcher(String matcher){
        if(!matcher.trim().equals(""))
        this.matchers += SEPERATOR + matcher;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public void setPositions(String positions) {
        this.positions = positions;
    }
    public String getCategories() {
        return categories;
    }

    public void setCategories(String categories) {
        this.categories = categories;
    }

    public String getPositions() {
        return positions;
    }


    public String getPositionCategories() {
        return positionCategories;
    }

    public void setPositionCategories(String positionCategories) {
        this.positionCategories = positionCategories;
    }

    public String getMatchers() {
        return matchers;
    }

    public void setMatchers(String matchers) {
        this.matchers = matchers;
    }


    public String[] toCsvRow(){
        return new String[] {
                this.categories,
                this.product,
                this.positions,
                this.positionCategories,
                this.matchers
        };
    }
}
