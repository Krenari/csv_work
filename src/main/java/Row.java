import java.util.Objects;

public class Row {

    private String category;
    private String product;
    private String position;
    private String positionCategory;
    private String matcher;

    public String getMatcher() {
        return matcher;
    }

    public void setMatcher(String matcher) {
        this.matcher = matcher;
    }


    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getPositionCategory() {
        return positionCategory;
    }

    public void setPositionCategory(String positionCategory) {
        this.positionCategory = positionCategory;
    }

    @Override
    public String toString() {
        return "Row{" +
                "category='" + category + '\'' +
                ", product='" + product + '\'' +
                ", position='" + position + '\'' +
                ", positionCategory='" + positionCategory + '\'' +
                ", matcher='" + matcher + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Row)) return false;
        Row that = (Row) o;
        return Objects.equals(getCategory(), that.getCategory()) &&
                Objects.equals(getProduct(), that.getProduct()) &&
                Objects.equals(getPosition(), that.getPosition()) &&
                Objects.equals(getPositionCategory(), that.getPositionCategory()) &&
                Objects.equals(getMatcher(), that.getMatcher());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getCategory(), getProduct(), getPosition(), getPositionCategory(), getMatcher());
    }


}
