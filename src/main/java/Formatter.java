import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Formatter {

    private List<Row> csvRows;


    public static final String[] headerRow = new String[]{
            "Categoria [Category]",
            "Prodotto [Product]",
            "Ordinamento dei prodotti di una categoria [Position]",
            "Ordinamento delle categorie per un prodotto [PositionCategory]",
            "matcher"
    };



    public Formatter() {
        csvRows = new ArrayList<Row>();
    }


    public void initialRowsFiller(String[] rowArray){


        Row row = new Row();
        String category = Util.checkNullValueAndAlwaysReturnValue(rowArray[0]);
        String product = Util.checkNullValueAndAlwaysReturnValue(rowArray[1]);
        String position = Util.checkNullValueAndAlwaysReturnValue(rowArray[2]);
        String positionCategory = Util.checkNullValueAndAlwaysReturnValue(rowArray[3]);
        String matcher = Util.checkNullValueAndAlwaysReturnValue(rowArray[4]);

        row.setCategory(category);
        row.setProduct(product);
        row.setPosition(position);
        row.setPositionCategory(positionCategory);
        row.setMatcher(matcher);

//        System.out.println(row.toString());
        csvRows.add(row);
    }


    public List<String[]> processOldRowsAndReturnToCsvRowNeededFormat(){

        Map<String,FormattedRow> processedRows =  processOldRows();
        List<String[]> formattedRows = new ArrayList<>();

        formattedRows.add(headerRow);

        for(String key : processedRows.keySet()){
            FormattedRow formattedRow = processedRows.get(key);
            String[] formatedArrayOfStrings = formattedRow.toCsvRow();
            formattedRows.add(formatedArrayOfStrings);
        }
        return formattedRows;
    }

    private Map<String,FormattedRow> processOldRows(){
        Map<String,FormattedRow> csvFormattedRows = new TreeMap<String,FormattedRow>();
        for(Row oldRow  : csvRows){
            String keyProduct = oldRow.getProduct();
            FormattedRow formattedRow = csvFormattedRows.get(keyProduct) ;
            if(formattedRow == null){
                formattedRow = new FormattedRow();
                formattedRow.setProduct(oldRow.getProduct());
                formattedRow.setCategories(oldRow.getCategory());
                formattedRow.setPositionCategories(oldRow.getPositionCategory());
                formattedRow.setPositions(oldRow.getPosition());
                formattedRow.setMatchers(oldRow.getMatcher());
            }
            else {
                formattedRow.addCategory(oldRow.getCategory());
                formattedRow.addPosition(oldRow.getPosition());
                formattedRow.addPositionCategories(oldRow.getPositionCategory());
                formattedRow.addMatcher(oldRow.getMatcher());

            }
            csvFormattedRows.put(keyProduct,formattedRow);
        }
        return csvFormattedRows;
    }






    public List<Row> getCsvRows(){
        return this.csvRows;
    }
}
